package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.Shape;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel{
    private IColorGrid IColorGrid;
    private static final double outerMargin = 30;
    private static final int xSize = 400;
    private static final int ySize = 300;
    private static final Color marginColor = Color.LIGHT_GRAY;

    public GridView(IColorGrid IColorGrid){
        this.IColorGrid = IColorGrid;
        this.setPreferredSize(new Dimension(xSize, ySize));
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGrid(g2);
    }

    // Draws a complete grid, including borders and squares 
    // alt innenfor det grå området i illustrasjonen). For å tegne rutene kaller på drawCells
    private void drawGrid(Graphics2D g2) {
        double width = xSize - 2*outerMargin;
        double height= ySize - 2*outerMargin;
        System.out.println(width);
        System.out.println(height);
        Graphics2D bG = g2;

        super.paintComponent(bG);
        Rectangle2D backGround = new Rectangle2D.Double(outerMargin, outerMargin, width, height); 
        bG.setColor(marginColor);
        bG.fill(backGround);

        CellPositionToPixelConverter cpPixel = new CellPositionToPixelConverter(backGround, this.IColorGrid, outerMargin);

        drawCells(bG, this.IColorGrid, cpPixel);
    }

    // Draws a collection of cells
    // For each cell, it calls getBoundsForCell to find location
    private static void drawCells(Graphics2D bG, CellColorCollection cells, CellPositionToPixelConverter cpPixel) {
        for (CellColor cell: cells.getCells()){
            Rectangle2D drawCell = cpPixel.getBoundsForCell(cell.cellPosition());
            if(cell.color() != null){
                bG.setColor(cell.color());
                bG.fill(drawCell);
            }
            else{
                bG.setColor(Color.DARK_GRAY);
                bG.fill(drawCell);
            }
        }
    }
}