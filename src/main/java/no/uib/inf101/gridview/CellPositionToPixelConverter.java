package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

// Converts a cells position to pixels
public class CellPositionToPixelConverter {
  private Rectangle2D box = new Rectangle2D.Double(0,0,0,0);
  private GridDimension gd;
  private Double margin;

  public CellPositionToPixelConverter(Rectangle2D rect, GridDimension gd, double db){
      this.box = rect;
      this.gd = gd;
      this.margin = db;
  }

   //Calculates location of a cell, calls CellToPixelConverter.getBoundsForCell
  public Rectangle2D getBoundsForCell(CellPosition cp) {
    // Calculate cell width and cell heigth 
    int nCols = this.gd.cols();
    int nRows = this.gd.rows();
    int col = cp.col();
    int row = cp.row();
    double height = this.box.getHeight();
    double width = this.box.getWidth();
    double startX = this.box.getX();
    double startY = this.box.getY();
    double margin = this.margin;

    double colSpace = (width - (margin*(nCols+1)));
    double rowSpace = (height - (margin*(nRows+1)));
    double cellWidth = colSpace/ nCols;
    double cellHeight = rowSpace/ nRows;

    // Calculate XY coordinate of cell
    double cellX = startX + ((col+1)*margin) + (col * cellWidth);
    double cellY = startY + ((row+1)*margin) + (row * cellHeight);

    Rectangle2D r2Bounds = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    return r2Bounds;
  }
}