package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  private final int rows;
  private final int columns;
  private Color [][] colorGrid;
  private List<CellColor> grid = new ArrayList<>();

  public ColorGrid(int rows, int columns) {
    this.rows = rows;
    this.columns = columns;
    this.colorGrid = new Color[rows][columns];
    
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.columns;
  }

  @Override
  public List<CellColor> getCells() {
    for (int i = 0; i < this.rows; i++){
      for (int j = 0; j < this.columns; j++){
        this.grid.add(new CellColor(new CellPosition(i, j), this.colorGrid[i][j]));
      }
    }
    return this.grid;
  }

  @Override
  public Color get(CellPosition pos) {
    int row = pos.row();
    int col = pos.col();
    Color cellColor = this.colorGrid[row][col];

    return cellColor;
  }
  
  @Override
  public void set(CellPosition pos, Color newColor) {
    int row = pos.row();
    int col = pos.col();
    
    this.colorGrid[row][col] = newColor;
    }
  }